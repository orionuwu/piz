import os
import sys

def restore_binary_data(input_path, output_path):
    with open(input_path, 'r', newline='\r\n') as input_file:
        character_string = input_file.read().replace('\r\n', ' ')
        binary_data = bytes.fromhex(character_string)

    with open(output_path, 'wb') as output_file:
        output_file.write(binary_data)

def restore_directory(directory_path, output_directory):
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    for root, _, files in os.walk(directory_path):
        for file in files:
            if file.endswith('.piz'):
                input_path = os.path.join(root, file)
                output_path = os.path.join(output_directory, os.path.splitext(file)[0])
                restore_binary_data(input_path, output_path)


def main():
    if len(sys.argv) < 3:
        print("Usage: python unpiz.py input_path output_path")
        return

    input_path = sys.argv[1]
    output_path = sys.argv[2]

    if os.path.isfile(input_path):
        restore_binary_data(input_path, output_path)
    elif os.path.isdir(input_path):
        restore_directory(input_path, output_path)
    else:
        print("Invalid input path.")

if __name__ == "__main__":
    main()
