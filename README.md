# Piz!

Have you ever felt like that .zip files are too useful? That your stored files are in too small of a format? Well that is where Piz comes in! It takes files, and pizzes all over them. As the files soak up the Piz they can expand upwards of 4 times the original file size.


## Usage

Piz has 2 programs, `piz.py` and `unpiz.py`, you specify a source file and then a target file in both. `piz` pizzes on the files, thus enlarging them, and `unpiz.py` removes the piz (like a sponge.)

`python piz.py foo.source bar.pizz` for example
