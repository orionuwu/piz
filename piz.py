import os
import sys

def process_binary_data(input_path, output_path):
            with open(input_path, 'rb') as input_file:
                binary_data = input_file.read()
        
            # Convert binary data to a string of characters with CRLF line endings
            character_string = ' '.join(format(byte, '02x') for byte in binary_data).replace(' ', '\r\n')
        
            # Save the character string to the output .piz file
            with open(output_path, 'w', newline='\r\n') as output_file:
                output_file.write(character_string)
        

def process_directory(directory_path, output_directory):
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    for root, _, files in os.walk(directory_path):
        for file in files:
            input_path = os.path.join(root, file)
            output_path = os.path.join(output_directory, f"{file}.piz")
            process_binary_data(input_path, output_path)


def main():
    if len(sys.argv) < 3:
        print("Usage: python piz.py input_path output_path")
        return

    input_path = sys.argv[1]
    output_path = sys.argv[2]

    if os.path.isfile(input_path):
        process_binary_data(input_path, output_path)
    elif os.path.isdir(input_path):
        process_directory(input_path, output_path)
    else:
        print("Invalid input path.")

if __name__ == "__main__":
    main()
